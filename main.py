from fastapi import FastAPI
import clickhouse_connect
import os

app = FastAPI()

CLICKHOUSE_HOST = os.getenv("CLICKHOUSE_HOST")
CLICKHOUSE_USER = os.getenv("CLICKHOUSE_USER")
CLICKHOUSE_PASSWORD = os.getenv("CLICKHOUSE_PASSWORD")
CLICKHOUSE_DATABASE = os.getenv("CLICKHOUSE_DATABASE")
CLICKHOUSE_QUERY_LIMIT = os.getenv("CLICKHOUSE_QUERY_LIMIT", "500")
CLICKHOUSE_CONNECT_TIMEOUT = os.getenv("CLICKHOUSE_CONNECT_TIMEOUT", "10")
CLICKHOUSE_SEND_RECEIVE_TIMEOUT = os.getenv("CLICKHOUSE_SEND_RECEIVE_TIMEOUT", "10")

client = clickhouse_connect.get_client(host=CLICKHOUSE_HOST,
                                       username=CLICKHOUSE_USER,
                                       password=CLICKHOUSE_PASSWORD,
                                       database=CLICKHOUSE_DATABASE,
                                       query_limit=CLICKHOUSE_QUERY_LIMIT,
                                       connect_timeout=CLICKHOUSE_CONNECT_TIMEOUT,
                                       send_receive_timeout=CLICKHOUSE_SEND_RECEIVE_TIMEOUT)

@app.post("/run")
async def run(request: dict):
    try:
        result = client.query(request["sql"].replace(";", ""))
    except Exception as e:
        return {
            "success": False,
            "data": {},
            "db": CLICKHOUSE_DATABASE,
            "error": {
                "message": str(e)
            }
        }

    response = {
        "success": True,
        "data": {
            "command": "SELECT",
            "oid": None,
            "rows": [{f"C{i}": col for i, col in enumerate(row)} for row in result.result_rows],
            "fields": [],
            "_parsers": [None for _ in result.column_names],
            "RowCtor": None,
            "rowAsArray": True,
            "rowCount": result.row_count,
        },
        "db": CLICKHOUSE_DATABASE
    }

    for i, column_name in enumerate(result.column_names):
        response["data"]["fields"].append({
            "name": column_name,
            "tableID": 16391,
            "columnID": i,
            "dataTypeID": 1043,
            "dataTypeSize": -1,
            "dataTypeModifier": 124,
            "format": "text"
        })

    return response
